# Minecraft-Management-System

Set of server modifications to allow a Minecraft server to be fully integrated with Systemd while allowing the server manager to keep access to the server console.  

Minecraft as a service is normally run interactively on the command line with `java -jar server.jar` and doesn't allow itself to be managed outside of its own interactive session.
Systemd integration, while normally great for most services, hides Minecraft's server console from us.
This project is a solution to this problem and allows easy Systemd management of the Minecraft service while giving a way for the server manager to access the Minecraft console.