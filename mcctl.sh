#!/bin/bash

#User must be part of the systemd-journal group and can be added by running "sudo usermod -aG systemd-journal <username>"
#
#Note that stopping, starting, and restarting the minecraftd.service should still be done via the systemctl commands

#Colors	
red='\e[31m'
yellow='\e[1;33m'
green='\e[0;32m'
default='\e[0m'
args="${@}"

function runcommand
{
	if [ $1 == "-q" ]; then #Quiet mode.  Just run the command dummy
		shift
		echo "${@}" > /opt/minecraft-server/console.in

	else
		cat /dev/null > /opt/minecraft-server/console.out
		echo -e "${red}Running command:${default} ${@}"
		echo "${@}" > /opt/minecraft-server/console.in
		echo -ne "${yellow}Server says:${default} "
		sleep 0.2
		cat /opt/minecraft-server/console.out
	fi

}

if [ $# -eq 0 ]; then
	echo -e "Enter ${red}${0} command${default} to send the command to the Minecraft server"
	echo -e "Enter ${red}${0} help${default} to view the Minecraft command help"
	exit
fi
if [ "${1}" == "help" ] || [ "${1}" == "/help" ] || [ "${1}" == "--help" ]; then
	if [ -z "${2}" ]; then #User just entered ./mcctl help
		echo "mcctl is a shell script to help manage a systemd integrated Minecraft server"
		echo ""
		echo -e "${red}mcctl custom commands:${default}"
		echo "reset    -- Wipes the current world"
		echo "log      -- View the recent server log"
		echo "-q flag  -- quiet command mode"
		echo "help     -- View this help"
		echo ""
		echo -e "${yellow}Minecraft server commands:${default}"
		echo "/advancement (grant|revoke)"
		echo "/ban <targets> [<reason>]"
		echo "/ban-ip <target> [<reason>]"
		echo "/banlist [ips|players]"
		echo "/bossbar (add|get|list|remove|set)"
		echo "/clear [<targets>]"
		echo "/clone <begin> <end> <destination> [filtered|masked|replace]"
		echo "/data (get|merge|modify|remove)"
		echo "/datapack (disable|enable|list)"
		echo "/debug (start|stop)"
		echo "/defaultgamemode (adventure|creative|spectator|survival)"
		echo "/deop <targets>"
		echo "/difficulty [easy|hard|normal|peaceful]"
		echo "/effect (clear|give)"
		echo "/enchant <targets> <enchantment> [<level>]"
		echo "/execute (align|anchored|as|at|facing|if|in|positioned|rotated|run|store|unless)"
		echo "/experience (add|query|set)"
		echo "/fill <from> <to> <block> [destroy|hollow|keep|outline|replace]"
		echo "/forceload (add|query|remove)"
		echo "/function <name>"
		echo "/gamemode (adventure|creative|spectator|survival)"
		echo "/gamerule (announceAdvancements|commandBlockOutput|disableElytraMovementCheck|doDaylightCycle|doEntityDrops|doFireTick|doLimitedCrafting|doMobLoot|doMobSpawning|doTileDrops|doWeatherCycle|keepInventory|logAdminCommands|maxCommandChainLength|maxEntityCramming|mobGriefing|naturalRegeneration|randomTickSpeed|reducedDebugInfo|sendCommandFeedback|showDeathMessages|spawnRadius|spectatorsGenerateChunks)"
		echo "/give <targets> <item> [<count>]"
		echo "/help [<command>]"
		echo "/kick <targets> [<reason>]"
		echo "/kill <targets>"
		echo "/list [uuids]"
		echo "/locate (Buried_Treasure|Desert_Pyramid|EndCity|Fortress|Igloo|Jungle_Pyramid|Mansion|Mineshaft|Monument|Ocean_Ruin|Pillager_Outpost|Shipwreck|Stronghold|Swamp_Hut|Village)"
		echo "/loot (give|insert|replace|spawn)"
		echo "/me <action>"
		echo "/msg <targets> <message>"
		echo "/op <targets>"
		echo "/pardon <targets>"
		echo "/pardon-ip <target>"
		echo "/particle <name> [<pos>]"
		echo "/playsound <sound> (ambient|block|hostile|master|music|neutral|player|record|voice|weather)"
		echo "/recipe (give|take)"
		echo "/reload"
		echo "/replaceitem (block|entity)"
		echo "/save-all [flush]"
		echo "/save-off"
		echo "/save-on"
		echo "/say <message>"
		echo "/schedule function <function> <time>"
		echo "/scoreboard (objectives|players)"
		echo "/seed"
		echo "/setblock <pos> <block> [destroy|keep|replace]"
		echo "/setidletimeout <minutes>"
		echo "/setworldspawn [<pos>]"
		echo "/spawnpoint [<targets>]"
		echo "/spreadplayers <center> <spreadDistance> <maxRange> <respectTeams> <targets>"
		echo "/stop"
		echo "/stopsound <targets> [*|ambient|block|hostile|master|music|neutral|player|record|voice|weather]"
		echo "/summon <entity> [<pos>]"
		echo "/tag <targets> (add|list|remove)"
		echo "/team (add|empty|join|leave|list|modify|remove)"
		echo "/teammsg <message>"
		echo "/teleport (<destination>|<location>|<targets>)"
		echo "/tell -> msg"
		echo "/tellraw <targets> <message>"
		echo "/time (add|query|set)"
		echo "/title <targets> (actionbar|clear|reset|subtitle|times|title)"
		echo "/tm -> teammsg"
		echo "/tp -> teleport"
		echo "/trigger <objective> [add|set]"
		echo "/w -> msg"
		echo "/weather (clear|rain|thunder)"
		echo "/whitelist (add|list|off|on|reload|remove)"
		echo "/worldborder (add|center|damage|get|set|warning)"
		echo "/xp -> experience"
	else
		runcommand ${args} #Run /help command... probably
	fi

elif [ "${1}" == "reset" ] || [ "${1}" == "/reset" ]; then
	echo -e "Stopping minecraft server"
	sudo systemctl stop minecraftd.service
	echo -e "${red}Removing world${default}"
	rm -rf /opt/minecraft-server/world* #Removes rand worlds too...
	echo -e "${yellow}Starting minecraft server${default}"
	sudo systemctl start minecraftd.service
	echo -e "${green}Server started!${default}"

elif [ "${1}" == "log" ] || [ "${1}" == "/log" ]; then
	journalctl -fu minecraftd.service --lines=30 #Have to be a part of the journal-users group for this one
	
else
	runcommand ${args}
fi
